[![Gem Version](https://badge.fury.io/rb/srv2env.svg)](http://badge.fury.io/rb/srv2env)

## use

     $ srv2env.rb _xmpp-server._tcp.google.com

Prints out : 


      export _XMPP_SERVER_HOST_0='xmpp-server.l.google.com'
      export _XMPP_SERVER_PORT_0='5269'
      export _XMPP_SERVER_PRIORITY_0='5'
      export _XMPP_SERVER_WEIGHT_0='0'
      export _XMPP_SERVER_HOST_1='alt2.xmpp-server.l.google.com'
      export _XMPP_SERVER_PORT_1='5269'
      export _XMPP_SERVER_PRIORITY_1='20'
      export _XMPP_SERVER_WEIGHT_1='0'
      export _XMPP_SERVER_HOST_2='alt3.xmpp-server.l.google.com'
      export _XMPP_SERVER_PORT_2='5269'
      export _XMPP_SERVER_PRIORITY_2='20'
      export _XMPP_SERVER_WEIGHT_2='0'
      export _XMPP_SERVER_HOST_3='alt1.xmpp-server.l.google.com'
      export _XMPP_SERVER_PORT_3='5269'
      export _XMPP_SERVER_PRIORITY_3='20'
      export _XMPP_SERVER_WEIGHT_3='0'
      export _XMPP_SERVER_HOST_4='alt4.xmpp-server.l.google.com'
      export _XMPP_SERVER_PORT_4='5269'
      export _XMPP_SERVER_PRIORITY_4='20'
      export _XMPP_SERVER_WEIGHT_4='0'

When put inside backticks :

     $ `./srv2env.rb _xmpp-server._tcp.generik.co.uk.`

makes shell interpret the output and adds the variables to current session 

     $ env | grep XMPP
     _XMPP_SERVER_HOST_0='phonebox.yllq.net'
     _XMPP_SERVER_PORT_0='5269'

