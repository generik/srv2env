
Gem::Specification.new do |s|
s.name        = 'srv2env'
s.version     = '0.1.0'
s.licenses    = ['GPL-2']
s.summary     = "DNS SRV to environment variable translator"
s.description = "Allows to export records from DNS SRV to environment variables
Usage: `srv2env.rb _myservice._protocol.example.com`"
s.authors     = ["Mateusz Pawlowski"]
s.email       = 'js@yllq.net'
s.files       = ["srv2env.rb"]
s.executables = ["srv2env.rb"]
s.bindir      = '.'
s.homepage    = 'https://bitbucket.org/generik/srv2env'
end
