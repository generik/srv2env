#!/usr/bin/env ruby
#
# Author: Mateusz Pawlowski <js@yllq.net>
# License : GPLv2
#

require 'resolv'

r = Resolv::DNS.new
domain=ARGV[0]
unless domain
	STDERR.puts "Please specify domain as an argument"
	exit 1
end
service= domain.split('.').first.gsub("-",'_')

resp = r.getresources(domain, Resolv::DNS::Resource::IN::SRV)

if resp.empty?
	STDERR.puts "No Resources for #{domain} found"
	exit 1
end

resp.sort_by! { |e| [e.priority, e.weight] }

resp.each_index do |index|
	puts "export #{service.upcase}_HOST_#{index}='#{resp[index].target}'"
	puts "export #{service.upcase}_PORT_#{index}='#{resp[index].port}'"
	puts "export #{service.upcase}_PRIORITY_#{index}='#{resp[index].priority}'"
	puts "export #{service.upcase}_WEIGHT_#{index}='#{resp[index].weight}'"
end
